#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 14."""

# Generate documentation: epydoc -v --html question_14.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

class MyClass(object):
    """Just a simple test class.

    The class I{MyClass} is composed of:
      - A constructor (I{__init__});
      - A I{display()} method.

    Note: In this case, the "too-few-public-methods" message raised by pylint
    can be ignored.
    """
    def __init__(self):
        """Constructor of I{MyClass}.

        @param self: Current instance of I{MyClass}.
        @type self: C{MyClass}
        """
        self.x_index = 23
        self.y_index = self.x_index+5

    def display(self, z_index=42):
        """This method displays the values of y and z_index.

        @param self: Current instance of I{MyClass}.

        @param z_index: Takes 42 as default value.

        @type self: C{MyClass}
        @type z_index: C{int}

        @raise z_index: TypeError, z_index must be a valid number (C{int}).
        """
        if not isinstance(z_index, int):
            raise TypeError('z_index must be a valid integer!')
        else:
            headers = ['y', 'z']
            data = [[self.y_index, z_index]]
            log.table(headers, data)

def main():
    """Create a I{MyClass} instance and use its I{display} method."""
    obj = MyClass()
    obj.display()

# Runtime processor
if __name__ == '__main__':
    main()
