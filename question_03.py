#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 3."""

# Generate documentation: epydoc -v --html question_03.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Sort words in lexicographic order.

    Note:
      1. A lowercase word is greater than an uppercase word according
         to ASCII comparison (e.g., C{'a' > 'A'} will return
         C{True} since C{ord('a') == 0x61} and C{ord('A') == 0x41});
      2. It's a bitwise process hence two words with starting with the
         same letter will be compared letter by letter.
    """
    words = []

    # Get two words from user input
    log.bold('Your words:')
    for i in range(1, 3):
        words += [input('[{i}] '.format(i=i))]  #: Add the word to words C{list}

    words.sort(key=str, reverse=False)  #: Sort words in place based on length
    # Alternative:
    # words = sorted(words, key=str)

    log.bold('Sorted words in lexicographic order:')
    for word in words:
        log.info('{word} -> {decomp}'.format(word=word,
                                             decomp=[(char, ord(char))
                                                     for char in word]))
    log.bold('{greater} is greater than {lower}.'.format(greater=words[1],
                                                         lower=words[0]))

# Runtime processor
if __name__ == '__main__':
    main()
