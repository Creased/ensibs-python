#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 7."""

# Generate documentation: epydoc -v --html question_07.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def my_function(x_value):
    """Return result of M{f(x)=2x^3+x-5} with C{x=x_value}.

    @param x_value: Number to process with M{f(x)=2x^3+x-5}.
    @type x_value: C{int}, C{float}
    @return: Return result of M{f(x)=2x^3+x-5} with C{x=x_value}.
    """
    res = pow((2*x_value), 3) + x_value - 5
    return res

def tabulate(callback, lower_bound, upper_bound, step):
    """Call a specified callable in a loop with specified range as args.

    @param callback: Function to call (use callable instead of `eval` and `exec`
                     for security reasons).
    @param lower_bound: Lower bound of loop.
    @param upper_bound: Upper bound of loop.
    @param step: Step of loop.

    @type callback: C{function}
    @type lower_bound: C{int}
    @type upper_bound: C{int}
    @type step: C{int}

    @raise callback: TypeError, function must be callable.
    @raise lower_bound: ValueError, the lower bound must be greater than the upper.
    @raise lower_bound: TypeError, lower_bound must be a valid number (C{int}).
    @raise upper_bound: TypeError, upper_bound must be a valid number (C{int}).
    @raise step: TypeError, step must be a valid number (C{int}).
    """
    if hasattr(callback, '__call__'):  #: DEBUG: C{dir(callback)}
        if not isinstance(lower_bound, int):
            raise TypeError('Lower bound must be a valid integer!')
        elif not isinstance(upper_bound, int):
            raise TypeError('Upper bound must be a valid integer!')
        elif not isinstance(step, int):
            raise TypeError('Step must be a valid integer!')
        else:
            if lower_bound > upper_bound:
                raise ValueError('Invalid boundaries, the lower bound must be '
                                 'greater than the upper!')
            else:
                for i in range(lower_bound, upper_bound, step):
                    result = callback(i)
                    log.bold(result)
    else:
        raise TypeError('Invalid callback, it must be callable!')

def main():
    """Call a function with user defined range as args.

    @raise start: ValueError, start must be a valid number (C{int}).
    @raise end: ValueError, end must be a valid number (C{int}).
    @raise step: ValueError, step must be a valid number (C{int}).
    @raise tabulate: ValueError or TypeError, invalid value specified as arg.
    """
    try:
        log.bold('Start (integer):')
        start = int(input('> '))
    except ValueError:
        log.error('Please enter a valid start!')

    try:
        log.bold('End (integer):')
        end = int(input('> '))
    except ValueError:
        log.error('Please enter a valid end!')

    try:
        log.bold('Step (integer):')
        step = int(input('> '))
    except ValueError:
        log.error('Please enter a valid step!')

    callback = my_function

    try:
        tabulate(callback, start, end, step)
    except (ValueError, TypeError) as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
