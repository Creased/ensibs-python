#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 16."""

# Generate documentation: epydoc -v --html question_16.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

class Vector2D(object):
    """This class allows to create a two-dimensional vector.

    Note: In this case, the "too-few-public-methods" message raised by pylint
    can be ignored.
    """
    def __init__(self, x_index=0, y_index=0):
        """Constructor of I{Vector2D}.

        @param self: Current instance of I{Vector2D}.
        @param x_index: Takes 0 as default value.
        @param y_index: Takes 0 as default value.

        @type self: C{Vector2D}
        @type x_index: C{int}
        @type y_index: C{int}

        @raise x_index: TypeError, x_index must be a valid number (C{int}).
        @raise y_index: TypeError, y_index must be a valid number (C{int}).
        """
        if not isinstance(x_index, int):
            raise TypeError('x must be a valid integer!')
        else:
            self.x_index = x_index

        if not isinstance(y_index, int):
            raise TypeError('y must be a valid integer!')
        else:
            self.y_index = y_index

    def display(self):
        """This method displays the values of x and y.

        @param self: Current instance of I{Vector2D}.
        @type self: C{Vector2D}
        """
        headers = ['x', 'y']
        data = [[self.x_index, self.y_index]]
        log.table(headers, data)

class ExtVector2D(Vector2D):
    """This class extends the Vector2D class.

    Note: In this case, the "too-few-public-methods" message raised by pylint
    can be ignored.
    """
    def __add__(self, vect2):
        """This method overloads the i{+} and C{__add__} method.

        @param self: Current instance of I{Vector2D}, the first vector of addition.
        @param vect2: Other instance of I{Vector2D}, the second vector of addition.

        @type self: C{Vector2D}
        @type vect2: C{Vector2D}

        @return: Returns the vector addition between C{self} and C{vect2}.
        """
        sum_ = Vector2D()  #: Instantiate a new empty vector
        sum_.x_index = self.x_index + vect2.x_index
        sum_.y_index = self.y_index + vect2.y_index
        return sum_

def main():
    """Create two vectors and print their addition.

    @raise x_index: TypeError, x_index must be a valid number (C{int}).
    @raise y_index: TypeError, y_index must be a valid number (C{int}).
    """
    vectors = []
    for i in range(0, 2):
        log.underline('Vector {i}:\n'.format(i=i+1))

        try:
            log.bold('Enter your x:')
            x_index = int(input('> '))
        except ValueError:
            log.error('Please enter a valid x!')

        try:
            log.bold('Enter your y:')
            y_index = int(input('> '))
        except ValueError:
            log.error('Please enter a valid x!')

        vectors += [ExtVector2D(x_index, y_index)]
        log.raw('\n')

    sum_ = vectors[0] + vectors[1]
    log.underline('Vector 1 + Vector 2:\n')
    sum_.display()

# Runtime processor
if __name__ == '__main__':
    main()
