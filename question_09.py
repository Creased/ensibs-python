#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 9."""

# Generate documentation: epydoc -v --html question_09.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Process a list and print results.

    Steps:
      1. Sort in place and display the list;
      2. Append an element with 12 as value and display the list;
      3. Reverse order and display the list;
      4. Display index with 17 as value from list;
      5. Pop element with 38 as value and display the list;
      6. Print the sublist from 2nd to 3rd element of the list;
      7. Print the sublist from 1st to 2nd element of the list;
      8. Print the sublist from 3rd to last element of the list;
      8. Print the sublist from first to last element of the list;
      8. Print the last element of the list using a negative index.
    """
    list_ = [17, 38, 10, 25, 72]

    log.underline("Step 1:")
    list_.sort()
    log.info(list_)

    log.underline("Step 2:")
    list_.append(12)
    log.info(list_)

    log.underline("Step 3:")
    list_.reverse()
    log.info(list_)

    log.underline("Step 4:")
    log.info(list_.index(17))

    log.underline("Step 5:")
    list_.pop(list_.index(38))
    log.info(list_)

    log.underline("Step 6:")
    log.info(list_[1:3])

    log.underline("Step 7:")
    log.info(list_[:2])

    log.underline("Step 8:")
    log.info(list_[2:])

    log.underline("Step 9:")
    log.info(list_[:])

    log.underline("Step 10:")
    log.info(list_[-1])

# Runtime processor
if __name__ == '__main__':
    main()
