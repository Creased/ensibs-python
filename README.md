# TP Python ENSIBS

L'objectif de ce TP est de s'initier à Python au travers de différents exercices.
En passant du traitement de la saisie utilisateur par manipulation de variables,
à la création de classe et à leur extension.
L'objectif est également de développer un regard critique sur l'utilisation de
conventions PEP (Python Enhancement Proposals).

## Installation

⚠ Ces scripts sont développés pour correspondre au maximum aux nouvelles exigences de
Python3, afin d'exploiter au mieux ces différents scripts, il est nécessaire de procéder à
l'installation de Python3 et de différents modules&nbsp;:

```bash
apt-get install python3
pip install -r requirements.txt
```

## Documentation

L'ensemble de la documentation est générée au moyen de Epydoc et accessible depuis
le répertoire [docs/](docs) (voir le fichier [docs/index.html](docs/index.html) pour accéder
à la table des matières.

