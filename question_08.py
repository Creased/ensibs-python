#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 8."""

# Generate documentation: epydoc -v --html question_08.py -o ./docs

from packages import log
from packages.time import pretty_time

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def tchacatchac(speed):
    """Use the speed of the train to determine the time left until the accident.

    @param speed: Speed of the train.
    @type speed: C{int}
    @raise speed: TypeError, speed must be a valid number (C{int} or C{float}).
    @return: Info about tchacatchac (e.g., time remaining).
    """
    if not isinstance(speed, int) and not isinstance(speed, float):
        raise TypeError('Speed must be a valid number (float or integer)!')
    elif speed > 0:
        time = int((170 / speed) * 3600)  #: Compute time and convert it to seconds
    else:
        time = 0

    hours = int(time / 3600)
    minutes = int((time % 3600) / 60)

    date_str = '{h}h'.format(h=9+hours)
    if minutes > 0:
        minutes_str = '0{m}m'.format(m=0+minutes)
        minutes_str = minutes_str[-3:]
        date_str += minutes_str

    speed_str = '{speed}km/h'.format(speed=speed)
    time_str = pretty_time(time)
    info = [speed_str, date_str, time_str]

    return info

def main():
    """Display table with information about an accident to come."""
    data = []

    for i in range(100, 300, 10):
        data += [tchacatchac(i)]

    headers = ['Speed', 'Time of accident', 'Remaining time']
    log.underline('Arrival times of the train in Arras station:\n')
    log.table(headers, data)

# Runtime processor
if __name__ == '__main__':
    main()
