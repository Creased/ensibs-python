#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 5."""

# Generate documentation: epydoc -v --html question_05.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Print integers between 0 and 15 with a step of 3."""
    for i in range(0, 15, 3):
        log.info(i)

# Runtime processor
if __name__ == '__main__':
    main()
