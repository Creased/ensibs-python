"""Logging package for Python.

This module helps scripts to display formatted text.  Colorization and
formatting are based on ANSI escape sequences.

"""

from __future__ import print_function
import sys

__all__ = ["bold", "underline",
           "debug", "info",
           "success", "error",
           "warn", "table", "raw"]

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

######################################################################
## Miscellaneous module data
######################################################################

#: Default color and format set, these can be replaced with any ANSI escape sequences.
COLORS = {
    'reset':     '\033[0m',
    'bold':      '\033[1m',
    'underline': '\033[4m',
    'black':     '\033[30m',
    'red':       '\033[31m',
    'green':     '\033[32m',
    'yellow':    '\033[33m',
    'blue':      '\033[34m',
    'magenta':   '\033[35m',
    'cyan':      '\033[36m',
    'white':     '\033[37m'
}

######################################################################
## Main Functions
######################################################################

def raw(string):
    """Print raw text.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{text}').format(text=string), end='')

def bold(string):
    """Print bold text.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}'
               '{text}{reset}').format(reset=COLORS['reset'],
                                       bold=COLORS['bold'],
                                       text=string))

def underline(string):
    """Print underlined text.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}{underline}'
               '{text}{reset}').format(reset=COLORS['reset'],
                                       bold=COLORS['bold'],
                                       underline=COLORS['underline'],
                                       text=string))

def debug(string):
    """Print formatted text for debugging purpose.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}{cyan}Debug:{reset} '
               '{white}{text}{reset}').format(reset=COLORS['reset'],
                                              bold=COLORS['bold'],
                                              cyan=COLORS['cyan'],
                                              white=COLORS['white'],
                                              text=string))

def info(string):
    """Print formatted text for info reporting purpose.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print((u'{reset}{bold}{white}\u279c{reset} '
               '{white}{text}{reset}').format(reset=COLORS['reset'],
                                              bold=COLORS['bold'],
                                              white=COLORS['white'],
                                              text=string))

def success(string):
    """Print formatted text for success reporting purpose.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}{white}[{green}+{white}]{reset} '
               '{white}{text}{reset}').format(reset=COLORS['reset'],
                                              bold=COLORS['bold'],
                                              white=COLORS['white'],
                                              green=COLORS['green'],
                                              text=string))

def error(string):
    """Print formatted text for error reporting purpose to stderr.

    See U{documentation<https://docs.python.org/3.0/whatsnew/3.0.html>} for
    C{sys.stderr} usage with C{print()}.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}{white}[{red}!{white}]{reset} '
               '{white}{text}{reset}').format(reset=COLORS['reset'],
                                              bold=COLORS['bold'],
                                              white=COLORS['white'],
                                              red=COLORS['red'],
                                              text=string), file=sys.stderr)
    exit(1)

def warn(string):
    """Print formatted text for warning purpose.

    @param string: The string to be displayed.
    @type string: C{str}
    """
    if string:
        print(('{reset}{bold}{white}[{yellow}?{white}]{reset} '
               '{white}{text}{reset}').format(reset=COLORS['reset'],
                                              bold=COLORS['bold'],
                                              white=COLORS['white'],
                                              yellow=COLORS['yellow'],
                                              text=string))

def get_max(array):
    """Return max length of specified data in order for padding generation.

    @param array: The table data.
    @type array: C{list}
    @return: The generated table paddings.
    """
    max_array = []
    for items in array:
        i = 0
        for item in items:
            if len(max_array) <= i:
                max_array += [0]
            max_array[i] = max(max_array[i], len(str(item)))
            i += 1

    return max_array

def add_line(data, paddings):
    """Return a line with specified data for table generation.

    @param data: The table data.
    @param paddings: The table paddings.

    @type data: C{list}
    @type paddings: C{list}

    @return: New line to add to table.
    """
    i = 0
    line = ''
    for item in data:
        line += '| {item}{pad}'.format(item=item,
                                       pad=(' '*((paddings[i]+1)-len(str(item)))))
        i += 1
    line += '|'

    return line

def add_sep(paddings):
    """Return a separator according to table paddings.

    @param paddings: The table paddings.
    @type paddings: C{list}
    @return: New separator to add to table.
    """
    line = ''
    for padding in paddings:
        line += '|{dash}'.format(dash='-'*(padding+2))
    line += '|'

    return line

def table(headers, data):
    """Print a textual table.

    @param data: The data of table.
    @param headers: The headers of table.

    @type data: C{list}
    @type headers: C{list}
    """
    paddings = get_max([headers] + data)

    lines = []
    lines += [add_line(headers, paddings)]
    lines += [add_sep(paddings)]

    for items in data:
        lines += [add_line(items, paddings)]

    for line in lines:
        bold(line)
