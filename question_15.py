#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 15."""

# Generate documentation: epydoc -v --html question_15.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

class Vector2D(object):
    """This class allows to create a two-dimensional vector.

    Note: In this case, the "too-few-public-methods" message raised by pylint
    can be ignored.
    """
    def __init__(self, x_index=0, y_index=0):
        """Constructor of I{Vector2D}.

        @param self: Current instance of I{Vector2D}.
        @param x_index: Takes 0 as default value.
        @param y_index: Takes 0 as default value.

        @type self: C{Vector2D}
        @type x_index: C{int}
        @type y_index: C{int}

        @raise x_index: TypeError, x_index must be a valid number (C{int}).
        @raise y_index: TypeError, y must be a valid number (C{int}).
        """
        if not isinstance(x_index, int):
            raise TypeError('x must be a valid integer!')
        else:
            self.x_index = x_index

        if not isinstance(y_index, int):
            raise TypeError('y must be a valid integer!')
        else:
            self.y_index = y_index

    def display(self):
        """This method displays the values of x and y.

        @param self: Current instance of I{Vector2D}.
        @type self: C{Vector2D}
        """
        headers = ['x', 'y']
        data = [[self.x_index, self.y_index]]
        log.table(headers, data)

def main():
    """Create two vectors and print their values.

    @raise x_index: TypeError, x_index must be a valid number (C{int}).
    @raise y_index: TypeError, y must be a valid number (C{int}).
    """
    log.underline('Vector 1:\n')

    try:
        log.bold('Enter your x:')
        x_index = int(input('> '))
    except ValueError:
        log.error('Please enter a valid x!')

    try:
        log.bold('Enter your y:')
        y_index = int(input('> '))
    except ValueError:
        log.error('Please enter a valid x!')

    vector1 = Vector2D(x_index, y_index)
    vector2 = Vector2D()

    log.underline('\nVector 1:\n')
    vector1.display()

    log.underline('\nVector 2:\n')
    vector2.display()

# Runtime processor
if __name__ == '__main__':
    main()
