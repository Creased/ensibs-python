#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 12."""

# Generate documentation: epydoc -v --html question_12.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Python C{set} processing.

    Steps:
       1. Display X;
       2. Display Y;
       3. Display if 'c' is in X;
       4. Display if 'a' is in Y;
       5. Display X - Y;
       6. Display Y - X;
       7. Display X union Y;
       8. Display X intersection Y.
    """
    set_x = {'a', 'b', 'c', 'd'}
    set_y = {'s', 'b', 'd'}

    log.bold('Initial elements:')
    log.info('X: {set}'.format(set=set_x))
    log.info('Y: {set}'.format(set=set_y))
    log.bold('Is \'c\' in X? {response}.'.format(response='Yes' if 'c' in set_x else 'No'))
    log.bold('Is \'a\' in Y? {response}.'.format(response='Yes' if 'a' in set_y else 'No'))
    log.bold('X - Y = {response}.'.format(response=set_x-set_y))
    log.bold('Y - X = {response}.'.format(response=set_y-set_x))
    log.bold(u'X \u222A Y = {response}.'.format(response=set_x.union(set_y)))
    log.bold(u'X \u2229 Y = {response}.'.format(response=set_x.intersection(set_y)))

# Runtime processor
if __name__ == '__main__':
    main()
