#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 4."""

# Generate documentation: epydoc -v --html question_04.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Ask for pressure and volume and check for threshold exceeding.

    @raise pressure: ValueError, pressure must be a valid number (C{int} or C{float}).
    @raise volume: ValueError, volume must be a valid number (C{int} or C{float}).
    @raise pressure: ValueError, volume must be lower than p_threshold.
    @raise volume: ValueError, volume must be lower than v_threshold.
    """
    p_threshold = 2.3
    v_threshold = 7.41

    try:
        log.bold('Enter your pressure:')
        pressure = float(input('> '))
    except ValueError:
        log.error('Please enter a valid pressure!')

    try:
        log.bold('Enter your volume:')
        volume = float(input('> '))
    except ValueError:
        log.error('Please enter a valid volume!')

    try:
        if pressure > p_threshold and volume > v_threshold:
            raise ValueError('Pressure and volume thresholds exceeded!')
        elif pressure > p_threshold:
            raise ValueError('Pressure threshold exceeded, '
                             'you should increase the volume!')
        elif volume > v_threshold:
            raise ValueError('Volume threshold exceeded, '
                             'you should decrease the volume!')
        else:
            log.success('All right!')
    except ValueError as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
