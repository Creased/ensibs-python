#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 13."""

# Generate documentation: epydoc -v --html question_13.py -o ./docs

from collections import Counter
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def word_count(string):
    """Count the number of occurrences per word from a string.

    @param string: Speed of the train.
    @type string: C{str}
    @raise string: TypeError, string must be a valid number C{str}.
    @return: Per word occurrences count.
    """
    words = str(string).split(' ')
    count = dict(Counter(words))

    return count

def main():
    """Display word occurrences from a string."""
    log.bold('Your string:')
    string = input('> ')

    count = word_count(string)

    log.bold(count)

# Runtime processor
if __name__ == '__main__':
    main()
