#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 10."""

# Generate documentation: epydoc -v --html question_10.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Add 3 to a [0-5] range if element is greater or equal to 2."""
    list_ = [i+3 if i >= 2 else i for i in range(6)]
    log.bold(list_)

# Runtime processor
if __name__ == '__main__':
    main()
