#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 11."""

# Generate documentation: epydoc -v --html question_11.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Print all possible two-letters combinations between two words."""
    words = ['abc', 'de']

    list_ = ['{0}{1}'.format(i, j) for i in words[0] for j in words[1]]
    log.bold(list_)

# Runtime processor
if __name__ == '__main__':
    main()
