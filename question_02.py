#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 2."""

# Generate documentation: epydoc -v --html question_02.py -o ./docs

from math import sqrt
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Display root of specified float using C{math.sqrt} module.

    @raise float_: ValueError, float_ must be a valid C{float}.
    @raise float_: ValueError, float_ must be positive or null.
    """
    try:
        log.bold('Enter your float (positive or null):')
        float_ = float(input('> '))
    except ValueError:
        log.error('Please enter a valid float!')

    try:
        if float_ >= 0:
            root = sqrt(int(float_))
            log.bold('Root of {float} is {root}.'.format(float=float_,
                                                         root=root))
        else:
            raise ValueError('Float must be positive or null!')
    except ValueError as exception_:
        log.error(exception_)

# Runtime processor
if __name__ == '__main__':
    main()
