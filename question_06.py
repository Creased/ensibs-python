#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 6."""

# Generate documentation: epydoc -v --html question_06.py -o ./docs

from math import pi
from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def cube(number):
    """Return cube of given number.

    @param number: Number to process.
    @type number: C{int}, C{float}
    @return: Return cube of given number.
    """
    cube_ = pow(number, 3)
    return cube_

def get_sphere_volume(radius):
    """Return the sphere volume for the given radius.

    @param radius: Radius of sphere.
    @type radius: C{int}, C{float}
    @return: Return volume based on the given radius.
    """
    volume = (4/3) * pi * cube(radius)
    return volume

def main():
    """Ask for radius and display sphere volume according to it.

    @raise radius: ValueError, radius must be a valid number (C{int} or C{float}).
    """
    try:
        log.bold('Enter your radius:')
        radius = float(input('> '))
    except ValueError:
        log.error('Please enter a valid radius!')

    volume = get_sphere_volume(radius)
    log.bold('The corresponding volume of sphere is {volume}'.format(volume=volume))

# Runtime processor
if __name__ == '__main__':
    main()
