#!/usr/bin/env python3
# -*- coding:Utf-8 -*-

"""Question 1."""

# Generate documentation: epydoc -v --html question_01.py -o ./docs

from packages import log

__author__ = 'Baptiste MOINE <contact@bmoine.fr>'
__version__ = '0.1-dev'
__date__ = '05 September 2017'

def main():
    """Compute and display time value based on M{time = distance / speed}).

    @raise distance: ValueError, distance must be a valid number (C{int} or C{float}).
    @raise speed: ValueError, speed must be a valid number (C{int} or C{float}).
    @raise speed: ZeroDivisionError, speed must be non-zero.
    """
    try:
        log.bold('Enter your distance:')
        distance = float(input('> '))
    except ValueError:
        log.error('Please enter a valid distance!')

    try:
        log.bold('Enter your speed (non-zero):')
        speed = float(input('> '))
    except ValueError:
        log.error('Please enter a valid speed!')

    try:
        time = distance / speed

        log.bold('The corresponding time is {time}'.format(time=time))
    except ZeroDivisionError:
        log.error('Please enter a non-zero speed!')

# Runtime processor
if __name__ == '__main__':
    main()
